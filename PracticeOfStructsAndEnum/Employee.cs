﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticeOfStructsAndEnum
{
    public struct Employee
    {
        public string Name { get; set; }
        public Vacancies Vacancies { get; set; }
        public int Salary { get; set; }
        public int[] EmploymentDate { get; set; }
    }
}
